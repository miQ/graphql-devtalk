class: middle
# GraphQL


---
## Agenda

1. What is this and why?
2. How does it work?
3. Queries
4. Frameworks



---
## Background

* created by Facebook in 2012
--

* created for describing the capabilities and requirements of data models for client‐server applications
--

* development of this standard started in 2015
--

* still evolving, latest specification is _Working Draft – October 2016_



---
## What is GraphQL?

* GraphQL is a query language for API
--

* it isn't tied to any specific database or storage engine



---
## Main idea

Get exactly what you ask for and nothing more.
---
class: notitle

```jql
{
  user(id: 4) {
    name
  }
}
```
--
```json
{
  "user": {
    "name": "Mark Zuckerberg"
  }
}
```
---
class: notitle

```jql
{
  user(id: 4) {
    id,
    name
  }
}
```
--
```json
{
  "user": {
    "id": 4,
    "name": "Mark Zuckerberg"
  }
}
```

---
## Many resources in single request

How to get all friend's names for user with ID: 4 ?

---
### With REST

* change _/users/4_ endpoint to include list of friend names
--

* request _/users/ID_ for each friend ID and get name from there
--

* add new endpoint for _/friends/4_ that returns names 

---
### With GraphQL

```jql
{
  user(id: 4) {
    name,
    friends {
      name
    }
  }
}
```
--
```json
{
  "user": {
    "name": "Mark Zuckerberg",
    "friends": [
      { "name": "John Smith" },
      { "name": "Ann Waltz" },
      { "name": "Jack Ripper" }
    ]
  }
}
```

---

### What if we want to also get a friends age?

--
```jql
{
  user(id: 4) {
    name,
    friends {
      name,
      age
    }
  }
}
```
--
```json
{
  "user": {
    "name": "Mark Zuckerberg",
    "friends": [
      { "name": "John Smith", "age": 25 },
      { "name": "Ann Waltz", "age": 33 },
      { "name": "Jack Ripper", "age": 65 }
    ]
  }
}
```

---
##Operation type & name

```jql
{
  user(id: 4) {
    name,
    friends {
      name
    }
  }
}
```
--
```jql
query UserNameAndFriends {
  user(id: 4) {
      name,
      friends {
        name
      }
    }
}
```

---
##Variables

```jql
query UserNameAndFriends {
  user(id: 4) {
      name,
      friends {
        name
      }
    }
}
```
--
```jql
query UserNameAndFriends($uId: Int) {
  user(id: $uId) {
      name,
      friends {
        name
      }
    }
}
```

---
##Mutations

```jql
mutation CreateUser($name: String!) {
  createUser(name: $name) {
    id
  }
}
```

---
class: middle

#Backend

---
##Defining schema

```jql
type Query {
  user: User
}

type User {
  name: String
  friends: [User]
  homeWorld: Planet
}

type Planet {
  name: String
  climate: String
}

type Mutation {
  createUser: User
}

```

---
## Resolvers

###How the query is resolved?

```jql
user(id: 4) {
  name,
  friends {
    name
  }
}
```

---
###Using resolvers - they define how to access each possible value in the graph structure

```jql
Query: {
  user(obj, args) {
    return db.loadUserByID(args.id).then(
      userData => new User(userData)
    )
  }
}
```
--

```jql
User: {
  name(obj, args) {
    return obj.name
  }
  friends(obj, args) {
    return db.loadUserFriends(obj.id)
  }
}
```

---
class: middle

## How to code?

---
### Node.js

_GraphQL.js_ 

[https://github.com/graphql/graphql-js]

### 
```bash
yarn add graphql express-graphql
```

---
### Create a schema

```jql
import { graphql, GraphQLSchema, 
    GraphQLObjectType, GraphQLString } from 'graphql';

const mySchema = new GraphQLSchema({
  query: new GraphQLObjectType({
    name: 'RootQueryType',
    fields: {
      hello: {
        type: GraphQLString,
        resolve() { return 'world'; }
      }
    }
  })
});
```

---
### Setup express endpoint

```javascript
const express = require('express');
const graphqlHTTP = require('express-graphql');

const app = express();

app.use('/graphql', graphqlHTTP({
  schema: mySchema
}));

app.listen(4000);
```

---
class: middle
## Relay

---
### Setup connection

```javascript
import { Environment, Network, 
  RecordSource, Store, } from 'relay-runtime';

function fetchQuery( operation, variables, ) {
  return fetch('/graphql', {
    method: 'POST',
    headers: { 'Content-Type': 'application/json', },
    body: JSON.stringify({
      query: operation.text,
      variables,
    }),
  }).then(response => {
    return response.json();
  });
}

export default new Environment({
  network: Network.create(fetchQuery),
  store: new Store(new RecordSource()),  
});
```

---
### Fetch data

```javascript
import {graphql, QueryRenderer} from 'react-relay';
//...
<QueryRenderer
    environment={environment}
    query={graphql`
      query UserQuery {
        viewer {
          id
        }  
      }
    `}
    variables={{}}
    render={({error, props}) => {
      if (error) { return <div>Error!</div>; }
      if (!props) { return <div>Loading...</div>; }
      return <div>User ID: {props.viewer.id}</div>;
    }}
/>
//...
```

---
### Mutate data

```javascript
//...
import {commitMutation, graphql} from 'react-relay';

const mutation = graphql`
  mutation MarkReadNotificationMutation(
    $input: MarkReadNotificationData!
  ) {
    markReadNotification(data: $input) {
      notification {
        seenState
      }
    }
  }
`;

//...
```

---
### Mutate data - commit the mutation

```javascript
//...
commitMutation(
     environment,
     {
         mutation,
         {
             input: {
                 source,
                 storyID,
             },
         },
         onCompleted: (response, errors) => {
             console.log('Response received from server.')
     },
         onError: err => console.error(err),
     },
 );
`;

//...
```

---
class: middle
## Apollo

---
### Setup connection 

```javascript
import { ApolloClient } from 'apollo-client';
import { HttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';

const client = new ApolloClient({
  link: new HttpLink({ uri: 'https://graphql.example.com' }),
  cache: new InMemoryCache()
});
```

--

```javascript
import { ApolloProvider } from 'react-apollo';

<ApolloProvider client={client}>
  <App />
</ApolloProvider>
```

---
### Fetch data

```javascript
function TodoApp({ data: { todos } }) {
  return (
    <ul>
      {todos.map(({ id, text }) => (
        <li key={id}>{text}</li>
      ))}
    </ul>
  );
}
```

--

```javascript
export default graphql(gql`
  query TodoAppQuery {
    todos {
      id
      text
    }
  }
`)(TodoApp);
```

---
### Mutate data

```javascript
//...
const submitRepository = gql`
  mutation submitRepository($repoFullName: String!) {
    submitRepository(repoFullName: $repoFullName) {
      createdAt
    }
  }
`;

const NewEntryWithData = graphql(submitRepository)(NewEntry);

//...
```

---
class: middle
## Graphiql

---
class: withimage

### https://github.com/graphql/graphiql

![](./graphiql.png)

---
## References

* http://facebook.github.io/graphql
* http://graphql.org/
* http://graphql.org/swapi-graphql/
* https://launchpad.graphql.com/new
* https://api.graph.cool/simple/v1/swapi
